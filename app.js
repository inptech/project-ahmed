const express = require("express");
const cors = require("cors");
const app = express();

const PORT = 3001;

const db = require("./models");

// const Role = db.role;

app.use(cors());

app.use(express.json());

app.use(express.urlencoded({
  extended: true
}));

const authRouter = require("./routes/auth.routes")

app.use("/auth", authRouter);

const userRouter = require("./routes/user.routes")

app.use("/test", userRouter);

const artikliRouter = require("./routes/artikli.route")

app.use("/artikli", artikliRouter);

const artcijRouter = require("./routes/artcij.route")

app.use("/artcij", artcijRouter);

const mulzagRouter = require("./routes/mulzag.route")

app.use("/mulzag", mulzagRouter);

const mulstavRouter = require("./routes/mulstav.route")

app.use("/mulstav", mulstavRouter);

const mgpzagRouter = require("./routes/mgpzag.route")

app.use("/mgpzag", mgpzagRouter);

const mgpstavRouter = require("./routes/mgpstav.route")

app.use("/mgpstav", mgpstavRouter);

const mfazagRouter = require("./routes/mfazag.route")

app.use("/mfazag", mfazagRouter);

const mfastavRouter = require("./routes/mfastav.route")

app.use("/mfastav", mfastavRouter);

const partneriRouter = require("./routes/partneri.route")

app.use("/partneri", partneriRouter);

const motzagRouter = require("./routes/motzag.route")

app.use("/motzag", motzagRouter);

const motstavRouter = require("./routes/motstav.route")

app.use("/motstav", motstavRouter);

const minzagRouter = require("./routes/minzag.route")

app.use("/minzag", minzagRouter);

const minstavRouter = require("./routes/minstav.route")

app.use("/minstav", minstavRouter);

const mnivelRouter = require("./routes/mnivel.route")

app.use("/mnivel", mnivelRouter);

db.sequelize.sync( /*{ force:true }*/ ).then(() => {
  console.log('Drop and Resync Db');
  //initial();
});

app.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}`);
})

// function initial() {
//   Role.create({
//     id: 1,
//     name: "user"
//   });

//   Role.create({
//     id: 2,
//     name: "admin"
//   });
// }