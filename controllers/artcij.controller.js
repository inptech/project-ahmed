const sql = require("mssql")
const sqlConfig = require("../config/ms.config")

exports.data = async (req, res) => {

        try {

            await sql.connect(sqlConfig.sqlConfig)
            var tableData = await sql.query(`SELECT * FROM [dbo].[artcij]`)
            var result = JSON.parse(JSON.stringify(tableData))
            res.status(200).send(JSON.stringify(result.recordset))

        } catch (err) {
            throw err;
        }

}