const db = require("../models");
const config = require("../config/auth.config");
const User = db.user;
const Role = db.role;

const Op = db.Sequelize.Op;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

exports.register = (req, res) => {

  const username = req.body.username
  const email = req.body.email
  const password = req.body.password
  const roles = req.body.roles

  User.create({
      username: username,
      email: email,
      password: bcrypt.hashSync(password, 8)
    })
    .then(user => {
      if (roles) {
        Role.findAll({
          where: {
            name: {
              [Op.or]: roles
            }
          }
        }).then(roles => {
          user.setRoles(roles).then(() => {
            res.send({
              message: "User registered successfully!"
            });
          });
        });
      } else {
        // user role = 1
        user.setRoles([1]).then(() => {
          res.send({
            message: "User registered successfully!"
          });
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: err.message
      });
    });

};

exports.login = (req, res) => {

  const username = req.body.username
  const password = req.body.password

  User.findOne({
      where: {
        username: username
      }
    })
    .then(user => {
      if (!user) {
        return res.status(404).send({
          message: "User Not Found"
        });
      }

      var passwordIsValid = bcrypt.compareSync(
        password,
        user.password
      );

      if (!passwordIsValid) {
        return res.status(401).send({
          accessToken: null,
          message: "Wrong username/password combination!"
        });
      }

      // var token = jwt.sign({ id: user.id }, config.secret, {
      //   expiresIn: 86400
      // });

      var authorities = [];
      user.getRoles().then(roles => {
        for (let i = 0; i < roles.length; i++) {
          authorities.push("ROLE_" + roles[i].name.toUpperCase());
        }
        var token = jwt.sign({
          id: user.id,
          roles: authorities
        }, config.secret, {
          expiresIn: 86400
        });
        res.status(200).send({
          /*data:*/
          id: user.id,
          username: user.username,
          email: user.email,
          roles: authorities,
          accessToken: token
        });
      });
    })
    .catch(err => {
      return res.status(500).send({
        message: err.message
      });
    });

}