const express = require("express");
const router = express.Router();
const partnericontroller = require("../controllers/partneri.controller");

router.get("/", partnericontroller.data)

module.exports = router