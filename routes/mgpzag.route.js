const express = require("express");
const router = express.Router();
const mgpzagcontroller = require("../controllers/mgpzag.controller");

router.get("/", mgpzagcontroller.data)

module.exports = router