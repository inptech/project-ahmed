const express = require("express");
const router = express.Router();
const mfastavcontroller = require("../controllers/mfastav.controller");

router.get("/", mfastavcontroller.data)

module.exports = router