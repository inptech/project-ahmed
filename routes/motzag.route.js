const express = require("express");
const router = express.Router();
const motzagcontroller = require("../controllers/motzag.controller");

router.get("/", motzagcontroller.data)

module.exports = router