const express = require("express");
const router = express.Router();
const mgpstavcontroller = require("../controllers/mgpstav.controller");

router.get("/", mgpstavcontroller.data)

module.exports = router