const express = require("express");
const router = express.Router();
const minzagcontroller = require("../controllers/minzag.controller");

router.get("/", minzagcontroller.data)

module.exports = router