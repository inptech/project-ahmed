const express = require("express");
const router = express.Router();
const mulzagcontroller = require("../controllers/mulzag.controller");

router.get("/", mulzagcontroller.data)

module.exports = router