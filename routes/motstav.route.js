const express = require("express");
const router = express.Router();
const motstavcontroller = require("../controllers/motstav.controller");

router.get("/", motstavcontroller.data)

module.exports = router