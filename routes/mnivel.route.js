const express = require("express");
const router = express.Router();
const mnivelcontroller = require("../controllers/mnivel.controller");

router.get("/", mnivelcontroller.data)

module.exports = router