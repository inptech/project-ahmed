const express = require("express");
const router = express.Router();
const minstavcontroller = require("../controllers/minstav.controller");

router.get("/", minstavcontroller.data)

module.exports = router